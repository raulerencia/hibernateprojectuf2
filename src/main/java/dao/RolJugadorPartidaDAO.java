package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.sun.xml.bind.v2.model.core.ID;

import hibernateProjectUF2.Partida;
import hibernateProjectUF2.Rol;
import hibernateProjectUF2.RolJugadorPartida;


public class RolJugadorPartidaDAO extends GenericDAO<RolJugadorPartida, ID> implements IRolJugadorPartidaDAO {

	public void descobrirRol(String userVidente, String userDescubierto, int IdPartida) {
		
		Session session = sessionFactory.getCurrentSession();
		
		boolean esVidente = false;
		Rol rol = null;
		boolean esNoche = false;
		
		try {
			session.beginTransaction();
			PartidaDAO pDAO = new PartidaDAO();
			List<Partida> partidas = pDAO.list();
			for (Partida p : partidas) {
				
				if (p.getIdPartida() == IdPartida) {
					
					if (p.getTorn() % 2 == 0) {
						
						esNoche = true;
						
					}
					break;
				}
			}
			
			if (esNoche) {
				
				RolJugadorPartidaDAO rjpDAO = new RolJugadorPartidaDAO();
				List<RolJugadorPartida> rjp = rjpDAO.list();
				
				for (RolJugadorPartida r : rjp) {
					
					if (r.getUser().getUserName() == userVidente) {
						
						if (r.getRol().getNom().equals("vidente")) {
							esVidente = true;
						}
						
					} else if (r.getUser().getUserName() == userDescubierto) {
						
						rol = r.getRol();
						
					}
				}
				
				if (esVidente) {
					
					System.out.println("Rol descubierto:");
					System.out.println(rol.getNom());
					
					
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
		}
		
		
	}
	
	
}
