package dao;

import hibernateProjectUF2.Partida;
import hibernateProjectUF2.User;

public interface IPartidaDAO extends IGenericDAO<Partida, String>{

	public void unirse(User u,int idPartida);
	
	public void inici(Partida p);
	
	public void jugadorsVius(int idPartida);
	
	public void rolsVius(int IdPartida);
	
	public void fiTorn(Partida p);
}
