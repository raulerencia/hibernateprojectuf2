package dao;

import com.sun.xml.bind.v2.model.core.ID;

import hibernateProjectUF2.RolJugadorPartida;

public interface IRolJugadorPartidaDAO extends IGenericDAO<RolJugadorPartida, ID>{

	public void descobrirRol(String userVidente, String userDescubierto, int IdPartida);
	
}
