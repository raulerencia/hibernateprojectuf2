package dao;

import hibernateProjectUF2.Mort;
import hibernateProjectUF2.Partida;

public interface IMortDAO extends IGenericDAO<Mort, String>{

	public void getMorts(int idPartida, int torn);
	
}
