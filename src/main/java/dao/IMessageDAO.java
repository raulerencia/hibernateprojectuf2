package dao;

import hibernateProjectUF2.Message;
import hibernateProjectUF2.Type;
import hibernateProjectUF2.User;

public interface IMessageDAO extends IGenericDAO<Message, String>{

	void enviaMissatge(String usernameSender,String usernameReceiver,Type type, String content);

	void getMissatges(String user);
	
}
