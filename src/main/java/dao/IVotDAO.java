package dao;

import com.sun.xml.bind.v2.model.core.ID;

import hibernateProjectUF2.Partida;
import hibernateProjectUF2.User;
import hibernateProjectUF2.Vot;

public interface IVotDAO extends IGenericDAO<Vot, ID>{

	void vota(String usernameSender, String usernameReceiver, int idPartida);
	
	void getHistorial(int idPartida, int torn);
}
