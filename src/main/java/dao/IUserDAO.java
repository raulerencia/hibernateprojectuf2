package dao;

import java.util.List;

import hibernateProjectUF2.User;

public interface IUserDAO extends IGenericDAO<User, String> {
	
	/*void saveOrUpdate(User m);

	User get(String id);

	List<User> list();

	void delete(String id);*/
	
	boolean registre(String userName, String password, String alias, String pathAvatar);
	
	boolean login(String userName, String password);
	
}
