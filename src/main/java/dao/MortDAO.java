package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import hibernateProjectUF2.Mort;
import hibernateProjectUF2.Partida;

public class MortDAO extends GenericDAO<Mort, String> implements IMortDAO{

	@Override
	public void getMorts(int idPartida, int torn) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			PartidaDAO pDAO = new PartidaDAO();
			List<Partida> partidas = pDAO.list();
			for (Partida p : partidas) {
				
				if (p.getIdPartida() == idPartida) {
					if (p.getTorn() == torn) {
						
						List<Mort> muertos = this.list();
						for (Mort m : muertos) {
							
							if (m.getTorn() == torn) {
								
								System.out.println(m.getUser().getUserName());
								
							}

						}
					}
				}
			}
			
			session.getTransaction().commit();
		} catch (HibernateException e) {
			
			e.printStackTrace();
			
			if (session != null && session.getTransaction() != null) {
				System.out.println("\nTransaccio no complerta, funcio getMorts");
				session.getTransaction().rollback();
			}
		}
		
	}

}
