package dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import hibernateProjectUF2.Mort;
import hibernateProjectUF2.Partida;
import hibernateProjectUF2.Rol;
import hibernateProjectUF2.RolJugadorPartida;
import hibernateProjectUF2.User;

public class PartidaDAO extends GenericDAO<Partida, String> implements IPartidaDAO {

	@Override
	public void unirse(User u, int idPartida) {

		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();

			List<Partida> partidas = this.list();

			UserDAO uDAO = new UserDAO();
			User newJugador = new User();
			ArrayList<User> usersDB = (ArrayList<User>) uDAO.list();

			for (User u1 : usersDB) {

				if (u1.getUserName().equals(u1.getUserName())) {

					newJugador = u1;

				}

			}

			for (Partida p : partidas) {

				if (p.getIdPartida() == idPartida) {

					if (p.getTorn() == 0) {

						Set<User> usersActius = new HashSet<User>();

						if (p.getUsers() != null) {
							usersActius = p.getUsers();
						}
						usersActius.add(newJugador);
						p.setUsers(usersActius);
						session.saveOrUpdate(p);
						session.saveOrUpdate(newJugador);
						session.getTransaction().commit();
						return;
					}

				}

			}
//
			ArrayList<User> usersActius = new ArrayList<User>();
			usersActius.add(newJugador);
			session.saveOrUpdate(newJugador);

			session.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();

			if (session != null && session.getTransaction() != null) {

				System.out.println("Transacció detinguda, jugador no agregat");
				session.getTransaction().rollback();

			}
		}

	}

	@Override
	public void inici(Partida p) {
		Session session = sessionFactory.getCurrentSession();

		try {

			Rol vilata = new Rol("Vilatà", "Vilatà. Vota pels damatins a qui sacrificar.", 0, "pathIMGVilatà");
			Rol llop = new Rol("Llop", "Llop. Per la nit decideix a qui matar. Pel dia pot votar a qui sacrificar.", 4,
					"pathIMGLlop");

			session.beginTransaction();

			session.saveOrUpdate(vilata);
			session.saveOrUpdate(llop);
			
			List<User> users = (List<User>) session.createQuery("FROM User").list();
			List <RolJugadorPartida> rjpl = new ArrayList<RolJugadorPartida>();

			for (int i = 1; i <= users.size(); i++) {
				if (i % llop.getFreq() == 0) {
					RolJugadorPartida rjp = new RolJugadorPartida(true, p, llop, users.get(i-1));
					rjpl.add(rjp);
					session.saveOrUpdate(rjp);
				} else {
					RolJugadorPartida rjp = new RolJugadorPartida(true, p, vilata, users.get(i-1));
					rjpl.add(rjp);
					session.saveOrUpdate(rjp);
				}
			}
			p.setTorn(0);
			session.saveOrUpdate(p);
			session.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}

	}

	@Override
	public void jugadorsVius(int idPartida) {
		Session session = sessionFactory.getCurrentSession();

		List<User> jugadorsVius = new ArrayList<User>();

		try {
			session.beginTransaction();
			
			List<RolJugadorPartida> rjp = (List<RolJugadorPartida>) session
					.createQuery("FROM RolJugadorPartida WHERE idpartida = " + idPartida).list();

			for (RolJugadorPartida rolJugadorPartida : rjp) {
				if (rolJugadorPartida.isViu()) {
					jugadorsVius.add(rolJugadorPartida.getUser());
				}
			}

			session.getTransaction().commit();

			System.out.println("\nJugadors vius:");

			if (jugadorsVius.size() > 0) {
				for (int i = 0; i < jugadorsVius.size(); i++) {
					System.out.println(jugadorsVius.get(i).getUserName());
				}
			} else {
				System.out.println("No hi ha jugadors vius\n");
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}

	}

	@Override
	public void rolsVius(int IdPartida) {

		ArrayList<Rol> alRolsVius = new ArrayList<Rol>();

		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();
			List<RolJugadorPartida> rjp = session.createQuery("FROM RolJugadorPartida WHERE idPartida= " + IdPartida)
					.list();

			for (RolJugadorPartida r : rjp) {
				if (r.isViu()) {
					alRolsVius.add(r.getRol());
				}
			}

			session.getTransaction().commit();

			for (int i = 0; i < alRolsVius.size(); i++) {

				System.out.println(alRolsVius.get(i).getNom());

			}

		} catch (HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();

		}

	}

	@Override
	public void fiTorn(Partida partida) {
		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();

			List<User> s = session.createQuery("SELECT receiver FROM Vot GROUP BY receiver ORDER BY receiver DESC")
					.list();
			for (User string : s) {
				Query query =  session.createQuery("FROM RolJugadorPartida WHERE userName='" + string.getUserName()+"'");
				List rjpl = query.list();
				RolJugadorPartida rjp = (RolJugadorPartida) rjpl.get(0); 
				rjp.setViu(false);
				session.saveOrUpdate(rjp);
				Mort m = new Mort(rjp.getUser(), partida, partida.getTorn());
				session.saveOrUpdate(m);
			}
			int torn = partida.getTorn()+1; 
			partida.setTorn(torn);
			session.merge(partida);
			session.getTransaction().commit();

		} catch (

		HibernateException e) {
			e.printStackTrace();
			session.getTransaction().rollback();

		}

	}

}
