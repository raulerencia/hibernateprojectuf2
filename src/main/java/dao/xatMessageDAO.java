package dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.sun.xml.bind.v2.model.core.ID;

import hibernateProjectUF2.Partida;
import hibernateProjectUF2.Rol;
import hibernateProjectUF2.RolJugadorPartida;
import hibernateProjectUF2.User;
import hibernateProjectUF2.xatMessage;

public class xatMessageDAO extends GenericDAO<xatMessage, ID> implements IxatMessageDAO{

	@Override
	public void getXat(int idPartida) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			List<xatMessage> xats = list();
			for (xatMessage xatMessage : xats) {
				if (xatMessage.getPartida().getIdPartida() == idPartida) {
					System.out.println(xatMessage.getSender().getUserName() + ": " + xatMessage.getContent());
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("No s'ha pogut accedir al xat");
				session.getTransaction().rollback();
			}
		}
	}

	@Override
	public void getXatLlops(int idPartida, String username) {
		Session session = sessionFactory.getCurrentSession();
		
		Partida partida = null;
		User user = null;
		Rol userRol = null;
		int torn = -1;
		
		try {
			session.beginTransaction();
			
			PartidaDAO pDAO = new PartidaDAO();
			List<Partida> partidas = pDAO.list();
			for (Partida p : partidas) {
				if (p.getIdPartida() == idPartida) {
					partida = p;
					torn = p.getTorn();
				}
			}
			
			UserDAO uDAO = new UserDAO();
			List<User> users = uDAO.list();
			for (User u : users) {
				if (u.getUserName().equals(username)) {
					user = u;
				}
			}
			
			RolJugadorPartidaDAO rjpDAO = new RolJugadorPartidaDAO();
			List<RolJugadorPartida> rjpL = rjpDAO.list();
			for (RolJugadorPartida r : rjpL) {
				
				if (r.getUser().equals(user)) {
					
					userRol = r.getRol();
					
				}
				
			}
			
			if(torn % 2 == 0) {
				if(userRol.getNom().equals("lobo")) {
					List<xatMessage> xats = this.list();
					for (xatMessage x : xats) {
						if(x.getPartida().getIdPartida() == idPartida) {
							System.out.println(x.getSender().getUserName()+ ": " + x.getContent());
						}	
					}
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("No s'ha pogut accedir al xat");
				session.getTransaction().rollback();
			}
		}
	}

	
	@Override
	public boolean escriureMissatgeXat(int idPartida, User sender, String content) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Partida partida = session.get(Partida.class, idPartida);
			if(partida.getTorn() % 2 != 0) {
				Date date = new Date();
				xatMessage message = new xatMessage(sender, partida, content, date);
				session.saveOrUpdate(message);
			}
			session.getTransaction().commit();
			System.out.println("Escriptura de missatge finalitzada correctament");
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			System.out.println("Escriptura de missatge finalitzada erroneament");
			return false;
		}
		return true;
	}

	@Override
	public boolean escriureMissatgeLlop(int idPartida, User sender, String content) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Partida partida = session.get(Partida.class, idPartida);
			if(partida.getTorn() % 2 == 0) {
				List<RolJugadorPartida> rjp = session.createQuery(
								"FROM RolJugadorPartida WHERE partida= " + idPartida + " AND userName = '" + sender.getUserName()+"'")
						.list();
				if(rjp != null && rjp.size() == 1) {
					if(rjp.get(0).getRol().getNom().equals("Llop")) {
						Date date = new Date();
						xatMessage message = new xatMessage(sender, partida, content, date);
						session.saveOrUpdate(message);	
					}
				}
			}
			session.getTransaction().commit();
		}catch(HibernateException e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
