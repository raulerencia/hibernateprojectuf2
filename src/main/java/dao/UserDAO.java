package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import hibernateProjectUF2.User;

public class UserDAO extends GenericDAO<User, String> implements IUserDAO {

	@Override
	public boolean login(String userName, String pw) {
		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();
			
			List<User> usersDB = this.list();

			for (User u : usersDB) {

				if (u.getUserName().equals(userName)) {

					if (u.getPassword().equals(pw)) {
						session.getTransaction().commit();
						return true;
					} else {
						session.getTransaction().commit();
						return false;
					}
				}

			}
		} catch (HibernateException e) {

			//e.printStackTrace();
			
			if (session != null && session.getTransaction() != null) {

				System.out.println("Transacció detinguda");
				session.getTransaction().rollback();

			}

		}
		return false;

		/*
		 * try { //session.beginTransaction(); boolean b;
		 * 
		 * User u = this.get(userName); System.out.println(u); if(u==null) { b = false;
		 * }else { //if(u.getPassword().equals(pw)) return true; else return false; b =
		 * (u.getPassword().equals(pw) ? true : false); }
		 * //session.getTransaction().commit(); return b; } catch (HibernateException e)
		 * { e.printStackTrace(); if (session != null && session.getTransaction() !=
		 * null) {
		 * System.out.println("\n.......Transaction Is Being Rolled Back.......");
		 * session.getTransaction().rollback(); } e.printStackTrace(); return false;
		 * 
		 * }
		 */
	}

	@Override
	public boolean registre(String userName, String password, String alias, String pathAvatar) {

		Session session = sessionFactory.getCurrentSession();

		User u = new User(userName, password, alias, pathAvatar);

		try {

			session.beginTransaction();
			session.saveOrUpdate(u);
			session.getTransaction().commit();
			return true;

		} catch (HibernateException e) {

			e.printStackTrace();

			if (session != null && session.getTransaction() != null) {

				System.out.println("Transacció detinguda");
				session.getTransaction().rollback();

			}

			return false;

		}

	}

}
