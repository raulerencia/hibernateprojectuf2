package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.sun.xml.bind.v2.model.core.ID;

import hibernateProjectUF2.Partida;
import hibernateProjectUF2.Rol;
import hibernateProjectUF2.RolJugadorPartida;
import hibernateProjectUF2.User;
import hibernateProjectUF2.Vot;

public class VotDAO extends GenericDAO<Vot, ID> implements IVotDAO{

	@Override
	public void vota(String usernameSender, String usernameReceiver, int idPartida) {
		Session session = sessionFactory.getCurrentSession();
		User sender = null, receiver = null;
		Partida partida = null;
		int torn = -1;
		Rol senderRol = null;
		try {
			session.beginTransaction();
			UserDAO uDao = new UserDAO();
			List<User> users = uDao.list();
			for (User u : users) {
				if(u.getUserName().equals(usernameSender)) sender = u;
				if(u.getUserName().equals(usernameReceiver)) receiver = u;
			}
			PartidaDAO pDao= new PartidaDAO();
			List<Partida> partidas = pDao.list();
			for (Partida p : partidas) {
				if(p.getIdPartida() == idPartida) {
					partida = p;
					torn = p.getTorn();
				}
			}
			RolJugadorPartidaDAO rjpDao = new RolJugadorPartidaDAO();
			List<RolJugadorPartida> listRjp = rjpDao.list();
			for(RolJugadorPartida rjp : listRjp) {
				if(rjp.getUser().equals(sender))
					senderRol = rjp.getRol();
			}
			
			if(torn % 2 == 0) {
				if(senderRol.getNom().equals("Llop")) {
					Vot v = new Vot(sender, receiver, partida, torn);
					session.saveOrUpdate(v);
				}
			}else {
				Vot v = new Vot(sender, receiver, partida, torn);
				session.saveOrUpdate(v);
			}
			session.getTransaction().commit();
		}catch(HibernateException e) {
			e.printStackTrace();
			if(session != null && session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
		}
	}

	@Override
	public void getHistorial(int idPartida, int torn) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			PartidaDAO p = new PartidaDAO();
			List<Partida> partidas = p.list();
			for (Partida partida : partidas) {
				if(partida.getIdPartida() == idPartida && partida.getTorn() == torn) {
					if(partida.getTorn() % 2 == 0){
						for (Vot vot : partida.getVot()) {
							System.out.println(vot.getSender().getUserName() + " ha votat a " + vot.getReceiver().getUserName());
						}
					}
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			if (session != null && session.getTransaction() != null) {
                System.out.println("La transacció ha estat cancelada");
                session.getTransaction().rollback();
            }
			e.printStackTrace();
		}
		
	}

	
}
