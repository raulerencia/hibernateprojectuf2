package dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import hibernateProjectUF2.Message;
import hibernateProjectUF2.Type;
import hibernateProjectUF2.User;

public class MessageDAO extends GenericDAO<Message, String> implements IMessageDAO{

	@Override
	public void enviaMissatge(String usernameSender, String usernameReceiver, Type type, String content) {

		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();

			User sender = null;
			User receiver = null;

			UserDAO uDAO = new UserDAO();

			List<User> usersDB = uDAO.list();

			for (User u : usersDB) {

				if (u.getUserName().equals(usernameSender)) {

					sender = u;

				}else if (u.getUserName().equals(usernameReceiver)) {

					receiver = u;

				}
			}
			
			if(sender != null && receiver != null) {
				
				Date fecha = new Date();
				Message m = new Message(sender, receiver, type, content, fecha);
				session.saveOrUpdate(m);
				
				System.out.println("\nMissatge enviat\n");
			}
			session.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();

			if (session != null && session.getTransaction() != null) {

				System.out.println("Transacció detinguda, missatge no enviat");
				session.getTransaction().rollback();

			}
		}

	}

	@Override
	public void getMissatges(String user) {
		Session session = sessionFactory.getCurrentSession();

		try {
			session.beginTransaction();

			List<Message> allMessages = this.list();
			ArrayList<String> mostrarMessages = new ArrayList<String>();
			
			for (Message m : allMessages) {
				
				if (m.getReceiver().getUserName().equals(user)) {
					
					mostrarMessages.add(m.toString());
					
				}else if (m.getSender().getUserName().equals(user)) {
					
					mostrarMessages.add(m.toString());
					
				}
				
			}
			
			System.out.println("----- MISSATGES -----\n");
			
			for (int i = 0; i < mostrarMessages.size(); i++) {
				
				System.out.println(mostrarMessages.get(i));
				System.out.println("\n");
			}
			session.getTransaction().commit();
			
		}catch (HibernateException e){
			e.printStackTrace();

			if (session != null && session.getTransaction() != null) {

				System.out.println("Transacció detinguda");
				session.getTransaction().rollback();

			}
		}
		
	}

}
