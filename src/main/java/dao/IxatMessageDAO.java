package dao;

import com.sun.xml.bind.v2.model.core.ID;

import hibernateProjectUF2.User;
import hibernateProjectUF2.xatMessage;

public interface IxatMessageDAO extends IGenericDAO<xatMessage, ID>{

	void getXat(int idPartida);
	void getXatLlops(int idPartida, String username);
	boolean escriureMissatgeXat(int idPartida, User sender, String content);
	boolean escriureMissatgeLlop(int idPartida, User sender, String content);
}
