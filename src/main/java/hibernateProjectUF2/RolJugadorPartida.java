package hibernateProjectUF2;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="RolJugadorPartida")
public class RolJugadorPartida {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column (name="id")
	private int id;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="userName")
	private User user;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="nom")
	private Rol rol;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="idPartida")
	private Partida partida;
	
	@Column(name="viu")
	private boolean viu;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Partida getPartida() {
		return partida;
	}

	public void setPartida(Partida partida) {
		this.partida = partida;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public RolJugadorPartida(boolean viu, Partida partida, Rol rol, User user) {
		super();
		this.user = user;
		this.rol = rol;
		this.partida = partida;
		this.viu = viu;
	}

	public RolJugadorPartida() {
	
	}
	
	
	
	
	
}
