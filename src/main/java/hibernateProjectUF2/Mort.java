package hibernateProjectUF2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Mort")
public class Mort {
	
	@Id
	@Column(name="id")
	int id;
	
	@ManyToOne
	@JoinColumn(name="userName")
	User user;
	
	@ManyToOne
	@JoinColumn(name="idPartida")
	Partida partida;
	
	@Column(name="torn")
	int torn;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Partida getPartida() {
		return partida;
	}

	public void setPartida(Partida partida) {
		this.partida = partida;
	}

	public int getTorn() {
		return torn;
	}

	public void setTorn(int torn) {
		this.torn = torn;
	}

	public Mort(User user, Partida partida, int torn) {
		super();
		this.user = user;
		this.partida = partida;
		this.torn = torn;
	}

	public Mort() {
		super();
	}

	
	
	
}
