package hibernateProjectUF2;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "User")
public class User implements Serializable {

	@Id
	@Column(name = "userName", unique = true, nullable = false)
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "alies")
	private String alies;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_registre")
	private Date dataRegistre;

	@Column(name = "path_avatar")
	private String pathAvatar;

	@Column(name = "percentatge_victories")
	private double percentatgeVictories;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	Set<RolJugadorPartida> rolJugPart = new HashSet<RolJugadorPartida>();

	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
	Set<Message> senderMess = new HashSet<Message>();

	@OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	Set<Message> receiverMess = new HashSet<Message>();

	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
	Set<Vot> senderVot = new HashSet<Vot>();

	@OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL)
	Set<Vot> receiverVot = new HashSet<Vot>();

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	Set<Mort> userMort = new HashSet<Mort>();

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "UserPartida", joinColumns = { @JoinColumn(name = "IdUser") }, inverseJoinColumns = {	@JoinColumn(name = "idPart") })
	private Set<Partida> partidas = new HashSet<Partida>();

	public User() {

	}

	public User(String userName, String password, String alias, String pathAvatar) {
		this.userName = userName;
		this.password = password;
		this.alies = alias;
		this.dataRegistre = new Date();
		this.pathAvatar = pathAvatar;
		this.percentatgeVictories = 0;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAlias() {
		return alies;
	}

	public void setAlias(String alias) {
		this.alies = alias;
	}

	public Date getDataRegistre() {
		return dataRegistre;
	}

	public void setDataRegistre(Date dataRegistre) {
		this.dataRegistre = dataRegistre;
	}

	public String getPathAvatar() {
		return pathAvatar;
	}

	public void setPathAvatar(String pathAvatar) {
		this.pathAvatar = pathAvatar;
	}

	public double getPercentatgeVictories() {
		return percentatgeVictories;
	}

	public void setPercentatgeVictories(double percentatgeVictories) {
		this.percentatgeVictories = percentatgeVictories;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", alies=" + alies + ", dataRegistre="
				+ dataRegistre + ", pathAvatar=" + pathAvatar + ", percentatgeVictories=" + percentatgeVictories
				+ ", partidas=" + partidas + "]";
	}

}
