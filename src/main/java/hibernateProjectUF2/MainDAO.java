package hibernateProjectUF2;

import dao.UserDAO;



public class MainDAO {
	
	public static void main(String[] args) {
		UserDAO uDAO = new UserDAO();		
		
		User u1=new User("username", "pass", "alias","pathAvatar");
		
		//A partir de ahi llamaremos a los DAO, haciendo las operaciones que nos permiten.
		//No tenemos que poreocuparnos de sesiones o transacciones, simplem,ente usar el DAO
		uDAO.login(u1.getUserName(), u1.getPassword());
	}
		

}
