package hibernateProjectUF2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.print.attribute.HashAttributeSet;

@Entity
@Table(name="Partida")
public class Partida implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPartida;
		
	@Column(name="torn")
	private int torn;
		
	@OneToMany(mappedBy="partida",cascade= {CascadeType.ALL})
	private Set<RolJugadorPartida> partida = new HashSet<RolJugadorPartida>();
	
	@OneToMany(mappedBy="partida",cascade= {CascadeType.ALL})
	private Set<Vot> vot = new HashSet<Vot>();
	
	@ManyToMany(mappedBy="partidas",cascade= {CascadeType.ALL})
	private Set<User> users = new HashSet<User>();

	public int getIdPartida() {
		return idPartida;
	}

	public void setIdPartida(int idPartida) {
		this.idPartida = idPartida;
	}

	public int getTorn() {
		return torn;
	}

	public void setTorn(int torn) {
		this.torn = torn;
	}

	public Set<RolJugadorPartida> getPartida() {
		return partida;
	}

	public void setPartida(Set<RolJugadorPartida> partida) {
		this.partida = partida;
	}

	public Set<Vot> getVot() {
		return vot;
	}
	
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public void setVot(Set<Vot> vot) {
		this.vot = vot;
	}

	public Partida(int torn) {
		this.torn = torn;
	}

	public Partida() {
		super();
	}

	
	
	
	
	
	
}
