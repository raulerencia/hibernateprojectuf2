package hibernateProjectUF2;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Rol")
public class Rol {

	@Id
	@Column(name = "nom")
	private String nom;

	@Column(name = "freq")
	private int freq;

	@Column(name = "pathImg")
	private String pathiImg;

	@Column(name = "descripcio")
	private String descripcio;

	@OneToMany(mappedBy = "rol", cascade = CascadeType.ALL)
	Set<RolJugadorPartida> rolJug = new HashSet<RolJugadorPartida>();

	public Rol(String nom, String pathiImg, int freq, String descripcio) {
		super();
		this.nom = nom;
		this.freq = freq;
		this.pathiImg = pathiImg;
		this.descripcio = descripcio;
	}
	public Rol() {
		
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getFreq() {
		return freq;
	}

	public void setFreq(int freq) {
		this.freq = freq;
	}

	public String getPathiImg() {
		return pathiImg;
	}

	public void setPathiImg(String pathiImg) {
		this.pathiImg = pathiImg;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public Set<RolJugadorPartida> getRolJug() {
		return rolJug;
	}

	public void setRolJug(Set<RolJugadorPartida> rolJug) {
		this.rolJug = rolJug;
	}

}
