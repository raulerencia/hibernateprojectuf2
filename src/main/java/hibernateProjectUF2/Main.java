package hibernateProjectUF2;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import dao.MessageDAO;
import dao.MortDAO;
import dao.PartidaDAO;
import dao.RolJugadorPartidaDAO;
import dao.UserDAO;
import dao.VotDAO;
import dao.xatMessageDAO;

public class Main {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			//
			// exception handling omitted for brevityaa

			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void main(String[] args) {
		PartidaDAO pDAO = new PartidaDAO();
		UserDAO uDAO = new UserDAO();
		User u = new User("user", "pass", "alias", "pathAvatar");

		// registre
		if (uDAO.registre("Raul", "raulpass", "aliasRaul", "raulpathAvatar")) {
			System.out.println("\n--- Usuari registrat --- \n");
		}
		;

		if (uDAO.registre("Ruben", "rubenpass", "aliasRuben", "rubenpathAvatar")) {
			System.out.println("\n--- Usuari registrat --- \n");
		}
		;

		if (uDAO.registre("Arnau", "arnaupass", "Arnauwu", "arnaupathAvatar")) {
			System.out.println("\n--- Usuari registrat --- \n");
		}
		;
		if (uDAO.registre("Jose", "josepass", "jose", "joseavatar")) {
			System.out.println("\n--- Usuari registrat --- \n");
		}
		;
		if (uDAO.registre("Dani", "danipass", "dani", "daniavatar")) {
			System.out.println("\n--- Usuari registrat --- \n");
		}
		;

		// login
		if (uDAO.login("Raul", "raulpass")) {
			System.out.println("\n--- Usuari loguejat --- \n");
		} else {
			System.out.println("login invalid");
		}

		if (uDAO.login("Ruben", "rubenpass")) {
			System.out.println("\n--- Usuari loguejat --- \n");
		} else {
			System.out.println("login invalid");
		}

		if (uDAO.login("Arnau", "arnaupass")) {
			System.out.println("\n--- Usuari loguejat --- \n");
		} else {
			System.out.println("login invalid");
		}

		if (uDAO.login("Arnau", "invalidPass")) {
			System.out.println("\n--- Usuari loguejat --- \n");
		} else {
			System.out.println("\nlogin invalid\n");
		}

		// enviarMissatge
		MessageDAO mDAO = new MessageDAO();

		mDAO.enviaMissatge("Raul", "Ruben", Type.TEXT, "Hola Ruben");
		mDAO.enviaMissatge("Arnau", "Raul", Type.TEXT, "Hola Raul");

		// getMissatges
		mDAO.getMissatges("Raul");

		// unirse
		Partida p = new Partida(0);
		
		xatMessageDAO XMDAO = new xatMessageDAO();
		User vilataArnau = uDAO.get("Arnau");
		User llopRaul = uDAO.get("Raul");
		pDAO.inici(p);

		pDAO.unirse(u, p.getIdPartida());

		// jugadorsVius
		pDAO.jugadorsVius(p.getIdPartida());

		// verRols
		pDAO.rolsVius(p.getIdPartida());

		pDAO.fiTorn(p);

		// vota
		VotDAO vDAO = new VotDAO();

		vDAO.vota("Raul", "Ruben", p.getIdPartida());
		
		pDAO.fiTorn(p);

		vDAO.getHistorial(p.getIdPartida(), p.getTorn());

		// getMorts
		MortDAO mortDAO = new MortDAO();
		mortDAO.getMorts(p.getIdPartida(), p.getTorn());
		
		XMDAO.escriureMissatgeLlop(p.getIdPartida(), llopRaul, "Hola, soc un vilata");

		pDAO.fiTorn(p);
		
		// descobrirRol
		RolJugadorPartidaDAO rjpDAO = new RolJugadorPartidaDAO();

		rjpDAO.descobrirRol("Raul", "Ruben", p.getIdPartida());
		
		XMDAO.escriureMissatgeXat(p.getIdPartida(), vilataArnau, "hola soc un llop");
		XMDAO.escriureMissatgeLlop(p.getIdPartida(), llopRaul, "Hola, soc un vilata eviant un missatge de nit");
		
		XMDAO.getXat(p.getIdPartida());
		XMDAO.getXatLlops(p.getIdPartida(), llopRaul.getUserName());

	}

}
